import { sql } from "@vercel/postgres";

export async function seed() {
  const createTable = await sql`
    CREATE TABLE IF NOT EXISTS users (
      id SERIAL PRIMARY KEY,
      name VARCHAR(255) NOT NULL,
      email VARCHAR(255) UNIQUE NOT NULL,
      image VARCHAR(255),
      "createdAt" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
    );
    `;

  console.log(`Created "users" table`);

  const users = await Promise.all([
    sql`
      INSERT INTO users (name, email, image)
      VALUES ('Mike Owino', 'mike@mikeowino.com', 'https://pbs.twimg.com/profile_images/1477684448569270272/uopS_eOt_400x400.jpg')
      ON CONFLICT (email) DO NOTHING;
  `,
  ]);
  console.log(`Seeded ${users.length} users`);

  return {
    createTable,
    users,
  };
}
